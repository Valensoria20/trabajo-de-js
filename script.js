//#region Variables
var num1 = 5;
let num2 = 7;
const valPi = 3.14;
//#endregion
//#region Funciones
const suma = (num1, num2) => num1 + num2;
const resta = (num1 ,num2) => num1 - num2;
const multp = (num1 ,num2) => num1*num2;
const div = (num1, num2) => num1/num2;
//#endregion
//#region Arreglo
let nombre = ['Valentin','Juan','Matias','Pedro',"Lucas"];
//#endregion
//#region Mostrar
console.log("El primer numero es: "+num1);
console.log("El segundo numero es: "+num2);
console.log("La constante de pi vale:"+valPi);
console.log("La suma es de: "+ suma(num1,num2));
console.log("La resta es de: "+ resta(num1,num2));
console.log("La multiplicacion es de: "+ multp(num1,num2));
console.log("La division es de: " + div(num1,num2));
for(cont=0;cont<5;cont++){
    console.log(nombre[cont]);
}
//#endregion